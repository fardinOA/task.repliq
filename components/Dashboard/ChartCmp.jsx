import React from "react";
import { Pie } from "react-chartjs-2";
import { useSelector } from "react-redux";
import "chart.js/auto";
const ChartCmp = () => {
    const { products } = useSelector(
        (state) => state.product.dashboardProducts
    );
    let outOfStock = 0;
    // calculate out of stock for all products
    products &&
        products.forEach((item) => {
            if (item.stock === 0) {
                outOfStock += 1;
            }
        });

    // data and configuration for Pie chart
    const pieState = {
        labels: ["Out of stock", "Instock"],
        datasets: [
            {
                data: [outOfStock, products && products.length - outOfStock],
                backgroundColor: ["#FFAEBC", "#B4F8C8"],
                hoverBorderColor: ["#FFAEBC", "#B4F8C8"],
                hoverBackgroundColor: ["#FFAEBC", "#B4F8C8"],
                hoverBorderWidth: 8,
                offset: 30,
                borderRadius: 10,
            },
        ],
    };
    // Options for Pie Chart
    const options = {
        cutoutPercentage: 60,
        responsive: true,
        legend: {
            display: true,
            position: "right",
        },

        //custom tooltip
        tooltips: {
            callbacks: {
                title: (items, data) => {
                    let x = Math.ceil(
                        data.datasets[items[0].datasetIndex].data[
                            items[0].index
                        ]
                    );
                    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                },
                label: (items, data) => data.labels[items.index],
            },

            backgroundColor: "#FFF",
            borderColor: "rgb(0,0,0)",
            titleFontSize: 14,
            titleFontColor: "#000",
            bodyFontColor: "#000",
            bodyFontSize: 10,
            displayColors: false,
        },
    };
    return (
        <div className=" mx-auto  ">
            <div className=" flex justify-center w-[300px] md:w-[400px] ">
                <Pie data={pieState} options={options} />
            </div>
        </div>
    );
};

export default ChartCmp;
