import Image from "next/image";
import React, { Fragment, useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import StarRatings from "react-star-ratings";
import Carousel from "framer-motion-carousel";

import { addToCart } from "../../redux/features/cart/cartSlice";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Head from "next/head";
const Details = ({ product }) => {
    const [quantity, setQuantity] = useState(1);

    const dispatch = useDispatch();

    const increaseQuantity = () => {
        if (product.stock <= quantity) return;
        setQuantity(quantity + 1);
    };
    const decreaseQuantity = () => {
        if (quantity === 1) return;
        setQuantity(quantity - 1);
    };

    useEffect(() => {}, []);

    return (
        <div className="   p-4 mt-[5rem] ">
            <Head>
                <title>{product?.name}</title>
            </Head>
            <div className=" flex flex-col justify-between space-y-8 md:space-y-0 md:flex-row md:space-x-4 ">
                <div className=" flex-1 h-full  flex justify-center items-center ">
                    <Carousel interval={3000} renderDots={false}>
                        {product &&
                            product?.images?.map((ele, ind) => (
                                <Image
                                    height={200}
                                    width={200}
                                    key={ele?.url}
                                    src={ele?.url}
                                    alt={`${ind + 1} Slide`}
                                    className=" w-[70%] mx-auto p-8  "
                                />
                            ))}
                    </Carousel>
                </div>
                <div className="    flex-1 ">
                    <div>
                        <h2 className=" md:text-left text-[1.3rem] text-center font-bold   text-secondary uppercase tracking-wider ">
                            {product?.name}
                        </h2>
                        <p>Product # {product?._id}</p>
                    </div>
                    <div className=" text-center md:text-left border-y-2 py-4 ">
                        <StarRatings
                            rating={product?.ratings}
                            starDimension="15px"
                            starSpacing=""
                            starRatedColor="tomato"
                        />
                        <span className="  ">
                            ({product?.numOfReviews}) Reviews
                        </span>
                    </div>
                    <h1 className=" text-[2rem] font-bold text-center md:text-left">
                        ${product?.price}
                    </h1>
                    <div className=" flex flex-col lg:flex-row lg:gap-4 justify-start items-center  ">
                        <div className="text-center  ">
                            <div className="  ">
                                <button
                                    className="bg-gray-500 text-center rounded-[5px] p-2 py-1 mr-2 text-[1rem] font-bold "
                                    onClick={decreaseQuantity}
                                >
                                    -
                                </button>
                                <input
                                    className="  max-w-[2rem] text-center border-none mx-auto  "
                                    readOnly
                                    value={quantity}
                                    type="number"
                                />
                                <button
                                    className="bg-gray-500 ml-2  rounded-[5px] p-2 py-1 text-[1rem] font-bold "
                                    onClick={increaseQuantity}
                                >
                                    +
                                </button>
                            </div>
                        </div>
                        <div className="flex justify-center  my-4">
                            <button
                                className=" w-[15rem] bg-[tomato] text-white font-bold text-center rounded-md py-1 px-2 mx-auto  "
                                disabled={product?.stock < 1 ? true : false}
                                onClick={() => {
                                    dispatch(
                                        addToCart({
                                            product: product._id,
                                            _id: product._id,
                                            name: product.name,
                                            price: product.price,
                                            image: product.images[0]?.url,
                                            stock: product.stock,

                                            quantity,
                                        })
                                    );
                                    toast.success(
                                        "Your product added to the cart",
                                        {
                                            position: "top-center",
                                            autoClose: 2000,
                                            hideProgressBar: false,
                                            closeOnClick: true,
                                            pauseOnHover: true,
                                            draggable: true,
                                            progress: undefined,
                                            theme: "light",
                                        }
                                    );
                                }}
                            >
                                <span> Add to Cart</span>
                                <span className=" "> </span>
                            </button>
                        </div>
                    </div>
                    <p className="font-bold text-[18px]">
                        Status:{" "}
                        <b className={`text-green-500`}>
                            {" "}
                            {product?.stock < 1 ? "OutOfStock" : "InStock"}
                        </b>
                    </p>
                    <div className="font-semibold">
                        Description:{" "}
                        <p className="font-normal text-[12px] ">
                            {product?.description}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Details;
