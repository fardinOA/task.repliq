import { motion } from "framer-motion";
import React from "react";

const SectionWrapper = (Component, idName) =>
    function HOC() {
        return (
            <motion.section
                variants={{
                    hidden: {},
                    show: {
                        transition: {
                            // staggerChildren: staggerChildren,
                            delayChildren: 0,
                        },
                    },
                }}
                initial="hidden"
                whileInView="show"
                viewport={{ once: true, amount: 0.25 }}
                className={` sm:px-16 px-6 sm:py-16 py-10  max-w-[80vw] mx-auto relative z-0 `}
            >
                <span className="hash-span">&nbsp;</span>
                <Component />
            </motion.section>
        );
    };

export default SectionWrapper;
