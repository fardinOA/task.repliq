import Head from "next/head";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
    fetchDashboardProducts,
    fetchOrders,
} from "../../redux/features/product/productSlice";
import AllOrders from "./AllOrders";
import AllProducts from "./AllProducts";
import CreateProduct from "./CreateProduct";
import DashboardSideBar from "./DashboardSideBar";
import DashboardView from "./DashboardView";
import OrderDetails from "./OrderDetails";

const Dashboard = () => {
    const [showProductBar, setShowProductBar] = useState(false);
    const [showProducts, setShowProducts] = useState(false);
    const [showDashboard, setShowDashboard] = useState(true);
    const [showAddProduct, setShowAddProduct] = useState(false);
    const [showOrders, setShowOrders] = useState(false);
    const [showOrderDetails, setShowOrderDetails] = useState(false);
    const [orderDetailsObj, setOrderDetailsObj] = useState({});
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchDashboardProducts());
        dispatch(fetchOrders());
    }, [dispatch]);

    return (
        <div className={` overflow-x-hidden mt-[5rem] relative `}>
            <Head>
                <title>Dashboard</title>
                <link rel="icon" href="/table-columns-solid.svg" />
            </Head>
            <div className=" w-full flex flex-col lg:flex-row   justify-between ">
                <div className="  w-full lg:w-[20%] px-8 ">
                    <DashboardSideBar
                        // delever function and state to control the viewing state
                        showProductBar={showProductBar}
                        setShowProductBar={setShowProductBar}
                        showProducts={showProducts}
                        setShowProducts={setShowProducts}
                        showDashboard={showDashboard}
                        setShowDashboard={setShowDashboard}
                        showAddProduct={showAddProduct}
                        setShowAddProduct={setShowAddProduct}
                        showOrders={showOrders}
                        setShowOrders={setShowOrders}
                        setShowOrderDetails={setShowOrderDetails}
                    />
                </div>
                <div className=" w-full lg:w-[80%]  relative  ">
                    {showProducts && <AllProducts />}
                    {showDashboard && <DashboardView />}
                    {showAddProduct && <CreateProduct />}
                    {showOrders && (
                        <AllOrders
                            setOrderDetailsObj={setOrderDetailsObj}
                            showOrders={showOrders}
                            setShowOrders={setShowOrders}
                            showOrderDetails={showOrderDetails}
                            setShowOrderDetails={setShowOrderDetails}
                        />
                    )}
                    {showOrderDetails && (
                        <OrderDetails
                            setShowOrderDetails={setShowOrderDetails}
                            setShowOrders={setShowOrders}
                            order={orderDetailsObj}
                        />
                    )}
                </div>
            </div>
        </div>
    );
};

export default Dashboard;
