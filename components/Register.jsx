import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { registerUser } from "../redux/features/user/userSlice";
import { useFormik } from "formik";
import { useEffect } from "react";
const Register = () => {
    const [avatar, setAvatar] = useState();
    const { error, isAuth } = useSelector((state) => state.user);
    const dispatch = useDispatch();
    const router = useRouter();

    const formik = useFormik({
        initialValues: {
            name: "",
            phone: "",
            password: "",
        },
        onSubmit: (values) => {
            dispatch(registerUser({ ...values, avatar }));
            router.push("/cart");
        },

        validate: (values) => {
            let errors = {};

            if (!values.password) {
                errors.password = "required";
            } else if (values.password.length < 8) {
                errors.password = "At least 8 characters or more";
            }

            if (!values.name) {
                errors.name = "required";
            }

            if (!values.phone) {
                errors.phone = "required";
            } else if (
                !/(^([+]{1}[8]{2}|0088)?(01){1}[3-9]{1}\d{8})$/.test(
                    values.phone
                )
            ) {
                errors.phone = "Invalid Bangladeshi phone number";
            }

            return errors;
        },
    });

    useEffect(() => {
        if (error) {
            toast.error(error, {
                position: "top-center",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });
        }
        if (isAuth) {
            router.push("/cart");
        }
    }, [error, isAuth, router]);

    const registerDataChange = (e) => {
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) {
                setAvatar(reader.result);
            }
        };
        reader.readAsDataURL(e.target.files[0]);
    };

    return (
        <div className="   h-screen w-screen flex justify-center items-center ">
            <div className="w-[80%]  h-[28rem] md:w-[20rem] flex border-1 rounded-md shadow-xl p-4 justify-center items-center ">
                <form
                    onSubmit={formik.handleSubmit}
                    className="mt-[2rem]  h-full w-full   "
                >
                    <h1 className=" text-[1.5rem] text-center border-b-2 border-blue-600 w-fit mx-auto ">
                        Register Here
                    </h1>
                    <div className=" flex flex-col ">
                        <label className=" text-[24px] " htmlFor="">
                            Name
                        </label>
                        <input
                            value={formik.values.name}
                            onChange={formik.handleChange}
                            type="text"
                            className=" border-[1px] rounded-md  px-2 text-[18px] outline-none "
                            placeholder="Name"
                            id="name"
                            name="name"
                        />
                        {formik.errors.name && (
                            <p className="text-red-700 ">
                                {formik.errors.name}
                            </p>
                        )}
                    </div>
                    <div className=" flex flex-col ">
                        <label className=" text-[24px] " htmlFor="">
                            Phone
                        </label>
                        <input
                            value={formik.values.phone}
                            onChange={formik.handleChange}
                            type="text"
                            className=" border-[1px] rounded-md  px-2 text-[18px] outline-none "
                            placeholder="Phone"
                            id="phone"
                            name="phone"
                        />
                        {formik.errors.phone && (
                            <p className="text-red-700 ">
                                {formik.errors.phone}
                            </p>
                        )}
                    </div>

                    <div className=" flex flex-col ">
                        <label className=" text-[24px] " htmlFor="">
                            Password
                        </label>
                        <input
                            value={formik.values.password}
                            onChange={formik.handleChange}
                            type="password"
                            className=" border-[1px] rounded-md  px-2 text-[18px] outline-none "
                            placeholder="Password"
                            id="password"
                            name="password"
                        />
                        {formik.errors.password && (
                            <p className="text-red-700 ">
                                {formik.errors.password}
                            </p>
                        )}
                    </div>

                    <div className=" flex flex-col ">
                        <label className=" text-[24px] " htmlFor="">
                            Avatar
                        </label>
                        <input
                            name="avatar"
                            onChange={registerDataChange}
                            type="file"
                            className="    px-2 text-[18px] outline-none "
                            placeholder="Password"
                        />
                    </div>
                    <button
                        type="submit"
                        className=" mt-4 bg-blue-500 w-full py-1 rounded-md text-white text-[20px] "
                    >
                        Register
                    </button>
                    <div className="flex">
                        <p>already have an account?</p>{" "}
                        <Link
                            href={`/login`}
                            className=" cursor-pointer text-[green] "
                        >
                            login here
                        </Link>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default Register;
