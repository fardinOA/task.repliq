import "../styles/globals.css";
import { store } from "../redux/store";
import { Provider, useDispatch, useSelector } from "react-redux";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Layout from "../components/Layout";
import { useEffect } from "react";
import { loadLoginUser } from "../redux/features/user/userSlice";
import NextNProgress from "nextjs-progressbar";
function MyApp({ Component, pageProps }) {
    useEffect(() => {
        store.dispatch(loadLoginUser());
    }, []);

    return (
        <Provider store={store}>
            <Layout>
                <NextNProgress
                    color="#29D"
                    startPosition={0.3}
                    stopDelayMs={200}
                    height={3}
                    showOnShallow={true}
                />
                <Component {...pageProps} />
                <ToastContainer
                    position="top-center"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="light"
                />
            </Layout>
        </Provider>
    );
}

export default MyApp;
