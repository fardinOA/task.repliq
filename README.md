# REPLIQ Assesment

This is an assessment, based on next js and tailwind CSS. This assessment is for a REPLIQ. Here i try to create a E-commerce web appliaction. For backend i use express and mongodb for database. This backend was created by me back in 2022. I still update the repo acroding my need.

## API Reference

#### Base Api

https://ecommerce-backend-black.vercel.app/api/v1

#### Get all products

```http
  GET /https://ecommerce-backend-black.vercel.app/api/v1/products
```

#### Get admin products

```http
  GET /https://ecommerce-backend-black.vercel.app/api/v1/admin/products
```

#### Get admin orders

```http
  GET /https://ecommerce-backend-black.vercel.app/api/v1/admin/orders
```

#### Add product

```http
  POST /https://ecommerce-backend-black.vercel.app/api/v1/admin/product/new
```

#### Login

```http
  POST /https://ecommerce-backend-black.vercel.app/api/v1/login-with-phone
```

#### Register

```http
  POST /https://ecommerce-backend-black.vercel.app/api/v1/register-with-phone
```

#### Load user

```http
  GET /https://ecommerce-backend-black.vercel.app/api/v1/me
```

#### Logout

```http
  GET /https://ecommerce-backend-black.vercel.app/api/v1/logout
```

#### New Order

```http
  POST /https://ecommerce-backend-black.vercel.app/api/v1/order/new
```

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/fardinOA/task.repliq.git
```

Go to the project directory

```bash
  cd task.repliq
```

Install dependencies

```bash
 yarn or npm i
```

Start the server

```bash
  yarn dev or npm run dev
```

## Demo

Visit the link to show the final output

https://task-repliq-afnan.vercel.app

## Support

For support, email afnan.eu.cse@gmail.com or visit https://fardin-me.vercel.app
