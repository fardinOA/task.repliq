import React from "react";

import dynamic from "next/dynamic";
// normal import have Hydration failed issue.
const Cart = dynamic(() => import("../../components/Cart"), {
    ssr: false,
});
const cart = () => {
    return (
        <>
            <Cart />
        </>
    );
};

export default cart;
