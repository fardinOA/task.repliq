import Image from "next/image";
import React, { Suspense } from "react";
import ImageLoader from "../ImageLoader";
import StarRatings from "react-star-ratings";
import Link from "next/link";
import { useDispatch } from "react-redux";
import { addToCart } from "../../redux/features/cart/cartSlice";
import { toast } from "react-toastify";
const Card = ({ product }) => {
    const dispatch = useDispatch();
    const addToCartHandeler = (ele) => {
        dispatch(
            addToCart({
                product: ele._id,
                _id: ele._id,
                name: ele.name,
                price: ele.price,
                image: ele.images[0]?.url,
                stock: ele.stock,
                quantity: 1,
            })
        );
        toast.success("Your product added to the cart", {
            position: "top-center",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
        });
    };
    return (
        <div className="mx-auto w-[22rem] rounded-md border-2 p-2 shadow-xl cursor-pointer hover:-translate-y-2 transition-all ">
            <figure className="  h-[15rem] w-full  border-b-2  ">
                <Suspense fallback={<ImageLoader />}>
                    <Image
                        height={100}
                        width={100}
                        src={product?.images[0]?.url}
                        alt={product?.name.slice(0, 10)}
                        className=" w-full  h-full object-contain  "
                    />
                </Suspense>
            </figure>
            <div className="  flex flex-col justify-between my-2 ">
                <div className=" flex justify-between items-center ">
                    <h2 className="  font-bold text-[15px] md:text-[18px]  ">
                        {product.name.slice(0, 25)}
                    </h2>
                    <StarRatings
                        rating={product.ratings}
                        starDimension="15px"
                        starSpacing=""
                        starRatedColor="tomato"
                    />
                </div>
                <div className="   flex justify-between mt-4 ">
                    <button
                        onClick={() => addToCartHandeler(product)}
                        className="  text-[16px] bg-[tomato] px-2 rounded-md text-white font-bold hover:scale-105 transition-all "
                    >
                        Add To Cart
                    </button>

                    <Link
                        className="text-[16px] bg-[tomato] px-2 rounded-md text-white font-bold hover:scale-105 transition-all "
                        href={`/product/${product._id}`}
                    >
                        View Details
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default Card;
