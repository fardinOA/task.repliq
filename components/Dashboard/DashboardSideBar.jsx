import React from "react";

const DashboardSideBar = ({
    setShowProductBar,
    showProductBar,
    setShowProducts,
    showProducts,
    showDashboard,
    setShowDashboard,
    showAddProduct,
    setShowAddProduct,
    showOrders,
    setShowOrders,
    setShowOrderDetails,
}) => {
    return (
        <div className="   w-[90%] lg:w-full   mx-auto   transition-all ">
            <div className=" w-full p-2 text-[18px] font-bold border-b-2 border-[tomato]  ">
                <p
                    className=" cursor-pointer "
                    onClick={() => {
                        // when dashboard are not in the view make it true & other false
                        !showDashboard && setShowDashboard(!showDashboard);
                        setShowProducts(false);
                        setShowAddProduct(false);
                        setShowOrders(false);
                        setShowOrderDetails(false);
                    }}
                >
                    Dashboard
                </p>
            </div>
            <div
                onClick={() => {
                    setShowProductBar(!showProductBar);
                }}
                className="cursor-pointer  p-2 text-[18px] font-bold flex border-b-2 border-[tomato]   justify-between "
            >
                <p>Product</p>{" "}
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-6 h-6  "
                >
                    <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M3 7.5L7.5 3m0 0L12 7.5M7.5 3v13.5m13.5 0L16.5 21m0 0L12 16.5m4.5 4.5V7.5"
                    />
                </svg>
            </div>
            {showProductBar && (
                <div className="    flex flex-col gap-2 text-[16px] font-semibold ">
                    <p
                        onClick={() => {
                            // when Products are not in the view make it true & other false

                            !showProducts && setShowProducts(!showProducts);
                            setShowDashboard(false);
                            setShowAddProduct(false);
                            setShowOrders(false);
                            setShowOrderDetails(false);
                        }}
                        className=" cursor-pointer hover:translate-x-2 transition-all my-2 flex gap-4 bg-gray-200 w-fit p-2 rounded-t-[5px] "
                    >
                        <span className=""> View All Product</span>
                        <span>
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-6 h-6"
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M7.5 3.75H6A2.25 2.25 0 003.75 6v1.5M16.5 3.75H18A2.25 2.25 0 0120.25 6v1.5m0 9V18A2.25 2.25 0 0118 20.25h-1.5m-9 0H6A2.25 2.25 0 013.75 18v-1.5M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                />
                            </svg>
                        </span>{" "}
                    </p>
                    <p
                        onClick={() => {
                            // when add product are not in the view make it true & other false

                            !showAddProduct &&
                                setShowAddProduct(!showAddProduct);
                            setShowDashboard(false);
                            setShowProducts(false);
                            setShowOrders(false);
                            setShowOrderDetails(false);
                        }}
                        className=" cursor-pointer hover:translate-x-2 transition-all flex gap-8 bg-gray-200 w-fit p-2 rounded-b-[5px] "
                    >
                        <span>Add a Product</span>
                        <span>
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-6 h-6"
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
                                />
                            </svg>
                        </span>{" "}
                    </p>
                </div>
            )}

            <div
                onClick={() => {
                    // when orders are not in the view make it true & other false

                    !showOrders && setShowOrders(!showOrders);
                    setShowAddProduct(false);
                    setShowDashboard(false);
                    setShowProducts(false);
                    setShowOrderDetails(false);
                }}
                className=" cursor-pointer  p-2 text-[18px] font-bold lg:border-b-2 border-[tomato]  "
            >
                Orders
            </div>
        </div>
    );
};

export default DashboardSideBar;
