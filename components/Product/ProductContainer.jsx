import React from "react";
import { useSelector } from "react-redux";
import SectionWrapper from "../HOC/SectionWrapper";
import Loader from "../Loader";
import Card from "./Card";

const ProductContainer = () => {
    const { products, isLoading } = useSelector((state) => state.product);
    return (
        <>
            {isLoading ? (
                <Loader />
            ) : (
                <div className="  flex flex-wrap gap-8  my-8">
                    {products?.products?.map((ele) => (
                        <Card product={ele} key={ele._id} />
                    ))}
                </div>
            )}
        </>
    );
};

export default SectionWrapper(ProductContainer, "products");
