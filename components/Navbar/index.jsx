import Image from "next/image";
import Link from "next/link";
import React, { Suspense, useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { logoutUser } from "../../redux/features/user/userSlice";

const Navbar = () => {
    const navLinks1 = [
        { title: "Home", to: "/" },
        { title: "Cart", to: "/cart" },
        { title: "Dashboard", to: "/dashboard" },
        { title: "Logout", to: "" },
    ];
    const navLinks2 = [
        { title: "Home", to: "/" },
        { title: "Cart", to: "/cart" },
        { title: "login", to: "/login" },
    ];

    const [toggle, setToggle] = useState(false);
    const [navLinks, setNavLinks] = useState(navLinks2);
    const { error, isAuth } = useSelector((state) => state.user);
    const { cartProducts } = useSelector((state) => state.cart);
    const dispatch = useDispatch();
    const clickHandler = (title) => {
        if (title === "Logout") {
            dispatch(logoutUser());
        }
    };

    useEffect(() => {
        if (isAuth) {
            setNavLinks(navLinks1);
        } else {
            setNavLinks(navLinks2);
        }
        if (error) {
            toast.error(error, {
                position: "top-center",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });
        }
    }, [error, isAuth]);

    return (
        <nav
            className={` w-full  bg-gray-200 flex items-center p-5  fixed top-0 z-[50] bg-primary `}
        >
            <div className="w-full flex justify-between items-center max-w-7xl mx-auto">
                <Link href={"/"} className="flex  items-center gap-2">
                    LOGO
                </Link>

                <ul className="list-none hidden sm:flex flex-row gap-10">
                    {navLinks.map((nav, ind) => (
                        <li
                            onClick={() => {
                                clickHandler(nav.title);
                            }}
                            key={`${ind}-${nav.title}`}
                            className={` ${
                                cartProducts.length &&
                                nav.title === "Cart" &&
                                "text-[tomato]"
                            }   text-[18px] font-medium cursor-pointer `}
                        >
                            <Link href={nav.to}>{nav.title} </Link>
                        </li>
                    ))}
                </ul>

                {/* {
                    <p className=" absolute top-[-.7rem] text-[tomato] font-bold  ">
                        {cartProducts.length}
                    </p>
                } */}

                <div className="sm:hidden  flex flex-1 justify-end items-center">
                    <Image
                        src={`${
                            toggle ? "/xmark-solid.svg" : "/bars-solid.svg"
                        }`}
                        width={150}
                        height={150}
                        alt="menu"
                        className="w-[28px] h-[28px] object-contain cursor-pointer "
                        onClick={() => setToggle(!toggle)}
                    ></Image>

                    <div
                        className={`${
                            !toggle ? "hidden" : "flex"
                        } p-6 black-gradient absolute top-20 right-0 mx-4 my-2 min-w-[140px] z-10 rounded-xl`}
                    >
                        <ul className="list-none bg-slate-400 p-4 rounded-md flex justify-end items-start flex-col gap-4 ">
                            {navLinks.map((nav, ind) => (
                                <li
                                    key={`${nav.title}-${ind}`}
                                    className="font-poppins font-medium cursor-pointer text-[16px] "
                                    onClick={() => {
                                        setToggle(!toggle);
                                        clickHandler(nav.title);
                                    }}
                                >
                                    <Link href={nav.to}>{nav.title}</Link>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    );
};

export default Navbar;
