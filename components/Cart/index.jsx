import Head from "next/head";
import Link from "next/link";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { addToCart, removeItem } from "../../redux/features/cart/cartSlice";
import CartItemCard from "./CartItemCard";

const Cart = () => {
    const { cartProducts } = useSelector((state) => state.cart);

    const dispatch = useDispatch();
    const increaseQuantity = (id, quantity, stock) => {
        const newQty = quantity + 1;
        if (stock <= quantity) {
            return;
        }
        // every time a quantity is increased, it will update the cart
        dispatch(addToCart({ _id: id }, newQty));
    };

    const decreaseQuantity = (id, quantity) => {
        const newQty = quantity - 1;

        if (1 >= quantity) {
            return;
        }
        // every time a quantity is decrease, it will update the cart

        dispatch(addToCart({ _id: id, actionType: "decreaseQuantity" }));
    };

    const deleteCartItems = (id) => {
        // delete items from cart
        dispatch(removeItem({ _id: id }));
    };

    return (
        <div className=" p-1 md:p-[5rem] mt-[5rem]  ">
            <Head>
                <title>Cart | Repliq Task </title>
                <link rel="icon" href="/cart-shopping-solid.svg" />
            </Head>
            {cartProducts.length > 0 ? (
                <>
                    <div className="bg-[tomato] p-4 w-full md:w-[90%] m-auto text-white flex justify-between ">
                        <p className=" w-[55%] ">Item</p>
                        <p className="  ">Quantity</p>
                        <p className="  ">Subtotal</p>
                    </div>
                    <section className="w-[100%] m-auto md:w-[90%] text-white flex flex-col  justify-between">
                        {cartProducts &&
                            cartProducts.map((item) => (
                                <div
                                    className="flex justify-between "
                                    key={item._id}
                                >
                                    {" "}
                                    <div className=" w-[50%] ">
                                        <CartItemCard
                                            item={item}
                                            deleteCartItems={deleteCartItems}
                                        />
                                    </div>
                                    <div className=" mt-[-.7rem] ml-2 flex items-center h-[8rem] ">
                                        <button
                                            className=" border-none text-white bg-gray-700 p-[.5rem] cursor-pointer "
                                            onClick={() =>
                                                decreaseQuantity(
                                                    item._id,
                                                    item.quantity
                                                )
                                            }
                                        >
                                            -
                                        </button>
                                        <input
                                            className="  w-[2rem] text-center border-none outline-none text-black "
                                            readOnly
                                            value={item.quantity}
                                            type="number"
                                        />
                                        <button
                                            className=" border-none text-white bg-gray-700 p-[.5rem] cursor-pointer "
                                            onClick={() =>
                                                increaseQuantity(
                                                    item._id,
                                                    item.quantity,
                                                    item.stock
                                                )
                                            }
                                        >
                                            +
                                        </button>
                                    </div>
                                    <p className=" text-black flex p-[.5rem] justify-end items-center  ">
                                        {item?.quantity * item?.price}
                                    </p>
                                </div>
                            ))}
                    </section>
                    <div className="mt-[1rem] flex flex-col md:flex-row justify-between text-center ">
                        <div></div>
                        <div>
                            <div className=" flex w-[80%] md:w-[60%] text-[25px]  justify-evenly border-t-2 border-[tomato] py-[1.2rem] m-2 mx-auto">
                                <p className="  ">Gross Total</p>
                                <p>
                                    {cartProducts?.reduce(
                                        (acc, item) =>
                                            acc + item.quantity * item.price,
                                        0
                                    )}
                                </p>
                            </div>

                            <div className="">
                                <Link
                                    disable={true}
                                    href={`/shipping`}
                                    className=" bg-[tomato] hover:bg-[#f54f32] transition-all text-white border-none py-[.8rem] px-[4rem] my-[1rem] mx-[4rem] cursor-pointer  "
                                >
                                    Check Out
                                </Link>
                            </div>
                        </div>
                    </div>
                </>
            ) : (
                <div className=" h-screen flex justify-center items-center ">
                    <div>
                        <h1 className=" text-red-500 text-[1.2rem] md:text-[2rem] font-bold ">
                            No products in cart
                        </h1>
                        <Link href={`/`}>
                            <p className=" mx-auto bg-blue-400 px-4 w-full py-1 rounded-sm text-center ">
                                {" "}
                                Go to home
                            </p>
                        </Link>
                    </div>
                </div>
            )}
        </div>
    );
};

export default Cart;
