import Head from "next/head";
import Image from "next/image";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../components/Loader";
import Card from "../components/Product/Card";
import ProductContainer from "../components/Product/ProductContainer";
import { fetchProducts } from "../redux/features/product/productSlice";

export default function Home() {
    const [posts, setPosts] = useState([]);
    const { products, isLoading } = useSelector((state) => state.product);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchProducts());
    }, [dispatch]);

    return (
        <>
            <Head>
                <title>Repliq Task</title>
                <link rel="icon" href="/dumpster-solid.svg" />
            </Head>
            <div className=" w-full  ">
                {" "}
                <ProductContainer />
            </div>
        </>
    );
}
