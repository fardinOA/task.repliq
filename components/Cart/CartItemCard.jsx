import Image from "next/image";
import Link from "next/link";
import React from "react";

const CartItemCard = ({ item, deleteCartItems }) => {
    return (
        <div className="flex p-[1rem]   h-[9rem] w-[6rem] items-start text-black ">
            <Image
                height={100}
                width={100}
                src={item?.image}
                alt={item?.name}
                className=" object-contain w-[5rem] "
            />
            <div className="flex m-[1rem] w-[5rem] flex-col ">
                {/* link to product details page */}
                <Link
                    className="mt-[.5rem]    max-w-full text-black "
                    href={`/product/${item?._id}`}
                >
                    {item?._id}
                </Link>
                <span>{`Price: ${item?.price}`}</span>
                <p
                    className=" text-[tomato] cursor-pointer "
                    onClick={() => deleteCartItems(item?._id)}
                >
                    Remove
                </p>
            </div>
        </div>
    );
};

export default CartItemCard;
