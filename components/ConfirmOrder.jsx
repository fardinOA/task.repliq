import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { clearCart, removeItem } from "../redux/features/cart/cartSlice";
import { addNewOrder } from "../redux/features/order/orderSlice";

const ConfirmOrder = () => {
    const dispatch = useDispatch();
    const { shippingInfo, cartProducts } = useSelector((state) => state.cart);
    const { user } = useSelector((state) => state.user.user);
    const { success } = useSelector((state) => state.order.order);
    const router = useRouter();
    const subtotal = cartProducts.reduce(
        (acc, item) => acc + item.price * item.quantity,
        0
    );

    const shippingCharges = subtotal > 1000 ? 0 : 200;
    const tax = subtotal * 0.05;
    const totalPrice = subtotal + shippingCharges + tax;
    const address = `${shippingInfo.address}, ${shippingInfo.city}, ${shippingInfo.state}, ${shippingInfo.pinCode}, ${shippingInfo.country}`;
    const orderInfo = {
        subtotal,
        shippingCharges,
        tax,
        totalPrice,
    };
    const orderData = {
        shippingInfo,
        orderItems: cartProducts,
        itemsPrice: orderInfo.subtotal,
        taxPrice: orderInfo.tax,
        shippingPrice: orderInfo.shippingCharges,
        totalPrice: orderInfo.totalPrice,
        paymentInfo: {
            id: Math.floor(100000000 + Math.random() * 900000000),
            status: "succeeded",
        },
    };
    const proceedToPayment = () => {
        dispatch(addNewOrder(orderData));
    };

    useEffect(() => {
        if (success) {
            toast.success("Your Order Is Placed", {
                position: "top-center",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });
            dispatch(clearCart());
            router.push("/cart");
        }
    }, [success, dispatch, router]);

    return (
        <Fragment>
            <div className="h-screen bg-inherit flex flex-col md:flex-row justify-between mt-[7rem] ">
                <div>
                    <div className=" p-[2rem] ">
                        <h2 className=" text-[1.5rem] p-[1rem] text-center border-b border-black w-[70%] m-auto ">
                            Shipping Info
                        </h2>
                        <div className=" m-[2rem] ">
                            <div className=" flex my-[1rem] ">
                                <p>Name:</p>
                                <span>{user?.name}</span>
                            </div>

                            <div className=" flex my-[1rem] ">
                                <p>Phone:</p>
                                <span>{user?.phoneNo}</span>
                            </div>

                            <div className=" flex my-[1rem] ">
                                <p>Address:</p>
                                <span>{address}</span>
                            </div>
                        </div>
                        <div className="confirmCartItems">
                            <h2 className=" text-[1.5rem] p-[1rem] text-center border-b border-black w-fit m-auto ">
                                Your Cart Items
                            </h2>
                            <div className="confirmCartItemsContainer">
                                {cartProducts &&
                                    cartProducts.map((item) => (
                                        <div
                                            className=" flex justify-between items-center my-[2rem]  "
                                            key={item.product}
                                        >
                                            <Image
                                                height={100}
                                                width={100}
                                                className=" w-[5rem] object-contain "
                                                src={item.image}
                                                alt="Product"
                                            />
                                            <Link
                                                href={`/product/${item.product}`}
                                            >
                                                {item.name.slice(0, 15)}{" "}
                                            </Link>
                                            <span>
                                                {item.quantity} x {item.price} ={" "}
                                                <b>
                                                    {item.price * item.quantity}
                                                </b>
                                            </span>
                                        </div>
                                    ))}
                            </div>
                        </div>
                    </div>
                </div>

                <div className=" md:border-l-2 border-black  ">
                    <div className=" p-[5rem] ">
                        <h2 className=" text-[1.5rem] p-[1rem] text-center border-b border-black w-full m-auto ">
                            Order Summery
                        </h2>
                        <div className=" border-b border-black ">
                            <div className=" flex justify-between m-[1rem] ">
                                <p className="  ">Subtotal:</p>
                                <span>{subtotal}</span>
                            </div>
                            <div className=" flex justify-between m-[1rem] ">
                                <p className="  ">Shipping Charges:</p>
                                <span>{shippingCharges}</span>
                            </div>
                            <div className=" flex justify-between m-[1rem] ">
                                <p className="  ">VAT:</p>
                                <span>{tax}</span>
                            </div>
                        </div>
                        <div className=" flex justify-between py-[2rem] border-t border-black ">
                            <p>
                                <b>Total:</b>
                            </p>
                            <span>{totalPrice}</span>
                        </div>
                        <button
                            className=" bg-[tomato] text-white w-full p-[1rem] m-auto outline-none rounded-[5px]"
                            onClick={proceedToPayment}
                        >
                            Proceed To Payment
                        </button>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default ConfirmOrder;
