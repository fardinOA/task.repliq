import Head from "next/head";
import React from "react";
import { useSelector } from "react-redux";

const AllProducts = () => {
    const { products } = useSelector(
        (state) => state.product.dashboardProducts
    );

    return (
        <div>
            <Head>
                <title>Dashboard | All Product</title>
            </Head>
            <table className="table w-full  overflow-x-auto ">
                <thead>
                    <tr className=" text-[17px] bg-[tomato] h-[5rem] ">
                        <th className=" text-left  w-[25%] ">Product Id</th>
                        <th className=" text-left w-[35%] ">Name</th>
                        <th className=" text-left w-[15%] ">Stock</th>
                        <th className=" text-left w-[15%] ">Price</th>
                    </tr>
                </thead>
                <tbody className=" w-full ">
                    {products?.map((product, ind) => (
                        <tr
                            className={`${ind % 2 ? "bg-red-100" : ""}`}
                            key={`dashboard-allProduct-${product._id}`}
                        >
                            <td className=" md:hidden pl-1 text-left py-1 text-[12px]  truncate  md:text-[16px] overflow-x-hidden   md:w-[20%] w-[12%]">
                                {product?._id.slice(0, 15)}
                            </td>
                            <td className="hidden md:inline-block pl-1 text-left py-1 text-[12px]      md:text-[16px] overflow-x-hidden w-full  ">
                                {product?._id}
                            </td>
                            {/*  */}
                            <td className="   pl-1 text-left py-1 text-[12px]  truncate  md:text-[16px] overflow-x-hidden    w-[12%]">
                                {product?.name.slice(0, 15)}
                            </td>

                            <td className=" text-left w-[15%]">
                                {product?.stock}
                            </td>

                            <td className=" text-left w-[15%]">
                                {product?.price}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default AllProducts;
