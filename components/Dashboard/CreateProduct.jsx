import Head from "next/head";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import {
    addNewProduct,
    clearNewProduct,
    fetchDashboardProducts,
} from "../../redux/features/product/productSlice";
const categories = [
    "laptop",
    "Computer",
    "Tops",
    "Smartphone",
    "T-Shirt",
    "Pant",
    "Shirt",
    "TV",
    "Sports",
];
const CreateProduct = () => {
    const dispatch = useDispatch();

    const { success, error, loading } = useSelector(
        (state) => state.product.newProduct
    );
    const [name, setName] = useState("");
    const [price, setPrice] = useState(0);
    const [description, setDescription] = useState("");
    const [category, setCategory] = useState("");
    const [stock, setStock] = useState(0);
    const [images, setImages] = useState([]);
    const [imagesPreview, setImagesPreview] = useState([]);
    useEffect(() => {
        if (success) {
            toast.success("New Product Created", {
                position: "top-center",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });

            dispatch(clearNewProduct());
            dispatch(fetchDashboardProducts());
        }
        if (error) {
            toast.error(error, {
                position: "top-center",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });
            dispatch(clearNewProduct());
        }
    }, [success, dispatch, error]);

    const createProductSubminHandler = (e) => {
        e.preventDefault();

        const myForm = new FormData();

        myForm.set("name", name);
        myForm.set("price", price);
        myForm.set("description", description);
        myForm.set("category", category);
        myForm.set("stock", stock);
        images.forEach((image) => {
            myForm.append("images", image);
        });

        dispatch(addNewProduct(myForm));
    };

    const createProductImageChange = (e) => {
        const files = Array.from(e.target.files);
        setImages([]);
        setImagesPreview([]);

        files.forEach((file) => {
            const reader = new FileReader();
            reader.onload = () => {
                if (reader.readyState === 2) {
                    setImagesPreview((old) => [...old, reader.result]);
                    setImages((old) => [...old, reader.result]);
                }
            };
            reader.readAsDataURL(file);
        });
    };
    return (
        <div className=" w-full flex flex-col items-center justify-center hscreen  ">
            <Head>
                <title>Dashboard | Add Product</title>
            </Head>
            <form
                className=" flex flex-col gap-4 justify-evenly items-center m-auto p-[3rem]   h-[90%]   "
                encType="multipart/form-data"
                onSubmit={createProductSubminHandler}
            >
                <h1 className=" text-[2rem] font-bold text-[tomato] ">
                    Create Product
                </h1>
                <div className=" flex w-full items-center ">
                    <input
                        className=" py-[1rem] px-[4rem] w-full border-2 outline-none border-inherit rounded-md "
                        type="text"
                        placeholder="Product Name"
                        required
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                </div>

                <div className=" flex w-full items-center ">
                    <input
                        className=" py-[1rem] px-[4rem] w-full border-2 outline-none border-inherit rounded-md "
                        type="number"
                        placeholder="Price"
                        required
                        value={price}
                        onChange={(e) => setPrice(e.target.value)}
                    />
                </div>

                <div className=" flex w-full items-center ">
                    <textarea
                        className=" py-[1rem] px-[4rem] w-full border-2 outline-none border-inherit rounded-md "
                        type="number"
                        placeholder="Product Description"
                        required
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                    />
                </div>

                <div className=" flex w-full items-center ">
                    <select
                        className=" py-[1rem] px-[4rem] w-full border-2 outline-none border-inherit rounded-md "
                        onChange={(e) => setCategory(e.target.value)}
                    >
                        <option value="">Choose Category</option>

                        {categories.map((item) => (
                            <option value={item} key={item}>
                                {item}
                            </option>
                        ))}
                    </select>
                </div>

                <div className=" flex w-full items-center ">
                    <input
                        className=" py-[1rem] px-[4rem] w-full border-2 outline-none border-inherit rounded-md "
                        type="number"
                        placeholder="Stock"
                        required
                        onChange={(e) => setStock(e.target.value)}
                    />
                </div>

                <div className=" flex p-[0%] ">
                    <input
                        className=" py-[1rem] px-[4rem] w-full border-2 outline-none border-inherit rounded-md "
                        type="file"
                        name="avatar"
                        accept="image/*"
                        multiple
                        onChange={createProductImageChange}
                    />
                </div>
                <div className=" w-full overflow-hidden ">
                    {imagesPreview.map((image, ind) => (
                        <Image
                            height={100}
                            width={100}
                            className=" w-[4rem] h-[4rem] object-contain rounded-full my-0 mx-[.5vmax] "
                            key={ind}
                            src={image}
                            alt="Product Preview"
                        />
                    ))}
                </div>

                <button
                    className="bg-[tomato] w-full text-white py-1 font-bold text-[18px] "
                    id="createProductBtn"
                    type="submit"
                    disabled={loading ? true : false}
                >
                    {loading ? "Loading..." : "Create"}
                </button>
            </form>
        </div>
    );
};

export default CreateProduct;
