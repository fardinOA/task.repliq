const { createSlice } = require("@reduxjs/toolkit");

const getFromLocalStorage = (key) => {
    if (!key || typeof window === "undefined") {
        return "";
    }
    return localStorage.getItem(key);
};

const initialState = {
    cartProducts: getFromLocalStorage("cartItems")
        ? JSON.parse(localStorage.getItem("cartItems"))
        : [],
    shippingInfo: getFromLocalStorage("shippingInfo")
        ? JSON.parse(localStorage.getItem("shippingInfo"))
        : {},
};

// create slice

const cartSlice = createSlice({
    name: "cart",
    initialState,
    reducers: {
        addToCart: (state, action) => {
            const itemInCart = state.cartProducts.find(
                (item) => item._id === action.payload._id
            );
            if (action.payload.actionType === "decreaseQuantity" && itemInCart)
                itemInCart.quantity--;
            else if (itemInCart) {
                itemInCart.quantity++;
            } else {
                state.cartProducts.push({ ...action.payload, quantity: 1 });
            }
            console.log(state.cartProducts);
            localStorage.setItem(
                "cartItems",
                JSON.stringify(state.cartProducts)
            );
        },

        removeItem: (state, action) => {
            const newItems = state.cartProducts.filter(
                (item) => item._id !== action.payload._id
            );

            state.cartProducts = newItems;

            localStorage.setItem(
                "cartItems",
                JSON.stringify(state.cartProducts)
            );
        },
        clearCart: (state, action) => {
            state.cartProducts = [];

            localStorage.setItem(
                "cartItems",
                JSON.stringify(state.cartProducts)
            );
        },
        saveShippingInfo: (state, action) => {
            state.shippingInfo = action.payload;

            localStorage.setItem(
                "shippingInfo",
                JSON.stringify(state.shippingInfo)
            );
        },
    },
});

export default cartSlice.reducer;
export const { addToCart, clearCart, removeItem, saveShippingInfo } =
    cartSlice.actions;
