import axios from "../../../utils/axios";

export const register = async (userData) => {
    const config = {
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "*",
        },
    };
    axios.defaults.withCredentials = true;

    const { data } = await axios.post(`/register-with-phone`, userData, config);

    return data;
};

export const login = async (loginData) => {
    const config = {
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "*",
        },
    };
    axios.defaults.withCredentials = true;

    const { data } = await axios.post(`/login-with-phone`, loginData, config);

    return data;
};

export const loadUser = async () => {
    try {
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*",
            },
        };
        axios.defaults.withCredentials = true;

        const { data } = await axios.get(`/me`, config);

        return data;
    } catch (error) {
        return { error: error.response.data.message };
    }
};

export const logout = async () => {
    try {
        await axios.get(`/logout`);
    } catch (error) {
        return error.response.data.message;
    }
};
