import axios from "../../../utils/axios";

export const newOrder = async (order) => {
    const config = {
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "*",
        },
    };
    axios.defaults.withCredentials = true;

    const { data } = await axios.post(`/order/new`, order, config);

    return data;
};
