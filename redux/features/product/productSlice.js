import {
    createProduct,
    getAllOrders,
    getAllProducts,
    getDashboardProducts,
} from "./productApi";

const { createAsyncThunk, createSlice } = require("@reduxjs/toolkit");

const initialState = {
    products: [],
    isLoading: false,
    isError: false,
    error: "",
    dashboardProducts: [],
    newProduct: {
        loading: false,
        error: "",
    },
    orders: [],
};

export const fetchProducts = createAsyncThunk(
    "product/fetchProducts",
    async () => {
        const products = await getAllProducts();
        return products;
    }
);

export const fetchDashboardProducts = createAsyncThunk(
    "product/fetchDashboardProducts",
    async () => {
        const products = await getDashboardProducts();
        return products;
    }
);

export const addNewProduct = createAsyncThunk(
    "product/addNewProduct",
    async (data) => {
        const product = await createProduct(data);

        return product;
    }
);

export const fetchOrders = createAsyncThunk("product/fetchOrders", async () => {
    const product = await getAllOrders();

    return product;
});

// create slice

const productSlice = createSlice({
    name: "product",
    initialState,
    reducers: {
        clearNewProduct: (state, action) => {
            state.newProduct = {};
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchProducts.pending, (state, action) => {
                state.isError = false;
                state.isLoading = true;
            })
            .addCase(fetchProducts.fulfilled, (state, action) => {
                state.isError = false;
                state.isLoading = false;
                state.products = action.payload;
            })
            .addCase(fetchProducts.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
                state.products = [];
            })
            .addCase(fetchDashboardProducts.pending, (state, action) => {
                state.isError = false;
                state.isLoading = true;
            })
            .addCase(fetchDashboardProducts.fulfilled, (state, action) => {
                state.isError = false;
                state.isLoading = false;
                state.dashboardProducts = action.payload;
            })
            .addCase(fetchDashboardProducts.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
                state.dashboardProducts = [];
            })
            .addCase(addNewProduct.pending, (state, action) => {
                state.newProduct.loading = true;
            })
            .addCase(addNewProduct.fulfilled, (state, action) => {
                state.newProduct.loading = false;
                state.newProduct = action.payload;
            })
            .addCase(addNewProduct.rejected, (state, action) => {
                state.newProduct.loading = false;
                state.newProduct.error = action.error?.message;
                state.newProduct = {};
            })
            .addCase(fetchOrders.pending, (state, action) => {
                state.isError = false;
                state.isLoading = true;
            })
            .addCase(fetchOrders.fulfilled, (state, action) => {
                state.isError = false;
                state.isLoading = false;
                state.orders = action.payload;
            })
            .addCase(fetchOrders.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
                state.orders = [];
            });
    },
});

export default productSlice.reducer;
export const { clearNewProduct } = productSlice.actions;
