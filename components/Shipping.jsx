import React, { Fragment, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Country, State } from "country-state-city";
import { saveShippingInfo } from "../redux/features/cart/cartSlice";
import { useRouter } from "next/router";

const Shipping = () => {
    const dispatch = useDispatch();
    const router = useRouter();
    const { cartProducts, shippingInfo } = useSelector((state) => state.cart);
    const [address, setAddress] = useState(shippingInfo.address);
    const [city, setCity] = useState(shippingInfo.city);
    const [state, setState] = useState(shippingInfo.state);
    const [country, setCountry] = useState(shippingInfo.country);
    const [pinCode, setPinCode] = useState(shippingInfo.pinCode);
    const [phoneNo, setPhoneNo] = useState(shippingInfo.phoneNo);
    const [loading, setLoading] = useState(true);
    const shippingSubmit = (e) => {
        e.preventDefault();
        if (phoneNo.length < 11 || phoneNo.length > 11) {
            alert.error("Phone Number Should be 11 digit long");
            return;
        }
        dispatch(
            saveShippingInfo({
                address,
                city,
                state,
                country,
                pinCode,
                phoneNo,
            })
        );
        router.push("/order/confirm");
    };

    return (
        <Fragment>
            <div className=" w-screen h-screen max-w-full flex justify-center items-center flex-col ">
                <div className=" mt-[8rem] bg-inherit max-w-[90vw] md:max-w-[45vw] h-[95vh] box-border overflow-hidden ">
                    <h2 className=" text-[1.5rem] p-[1rem] text-center border-b border-black w-[70%] m-auto ">
                        Shipping Details
                    </h2>

                    <form
                        encType="multipart/form-data"
                        className=" flex flex-col justify-evenly items-center p-[2rem] m-auto h-[80%] transition-all duration-300 "
                        onSubmit={shippingSubmit}
                    >
                        <div className=" flex w-full items-center ">
                            <input
                                className=" py-[1rem] px-[4rem] w-full box-border border border-gray-400
                              rounded-[4px] outline-none "
                                type="text"
                                placeholder="Address"
                                required
                                value={address}
                                onChange={(e) => setAddress(e.target.value)}
                            />
                        </div>

                        <div className=" flex w-full items-center ">
                            <input
                                className=" py-[1rem]  px-[4rem] w-full box-border border border-gray-400
                              rounded-[4px] outline-none "
                                type="text"
                                placeholder="City"
                                required
                                value={city}
                                onChange={(e) => setCity(e.target.value)}
                            />
                        </div>

                        <div className=" flex w-full items-center ">
                            <input
                                className=" py-[1rem] px-[4rem] w-full box-border border border-gray-400
                              rounded-[4px] outline-none "
                                type="number"
                                placeholder="Pin Code"
                                required
                                value={pinCode}
                                onChange={(e) => setPinCode(e.target.value)}
                            />
                        </div>

                        <div className=" flex w-full items-center ">
                            <input
                                className=" py-[1rem] px-[4rem] w-full box-border border border-gray-400
                              rounded-[4px] outline-none "
                                type="number"
                                placeholder="Phone Number"
                                required
                                value={phoneNo}
                                onChange={(e) => setPhoneNo(e.target.value)}
                            />
                        </div>

                        <div className=" flex w-full items-center ">
                            <select
                                className=" py-[1rem] px-[4rem] w-full box-border border border-gray-400
                              rounded-[4px] outline-none "
                                required
                                value={country}
                                onChange={(e) => setCountry(e.target.value)}
                            >
                                <option value="">Country</option>
                                {Country &&
                                    Country.getAllCountries().map((item) => (
                                        <option
                                            key={item.isoCode}
                                            value={item.isoCode}
                                        >
                                            {item.name}
                                        </option>
                                    ))}
                            </select>
                        </div>
                        {country && (
                            <div className=" flex w-full items-center ">
                                <select
                                    className=" py-[1rem] px-[4rem] w-full box-border border border-gray-400
                              rounded-[4px] outline-none "
                                    required
                                    value={state}
                                    onChange={(e) => setState(e.target.value)}
                                >
                                    <option value="">State</option>
                                    {State &&
                                        State.getStatesOfCountry(country).map(
                                            (item) => (
                                                <option
                                                    key={item.isoCode}
                                                    value={item.name}
                                                >
                                                    {item.name}
                                                </option>
                                            )
                                        )}
                                </select>
                            </div>
                        )}
                        <input
                            type="submit"
                            value="Continue"
                            className=" bg-[tomato] w-full p-4 transition-all cursor-pointer text-[1.2rem] text-white font-bold hover:bg-[#f15034]    rounded-[5px]"
                            disabled={state ? false : true}
                        ></input>
                    </form>
                </div>
            </div>
        </Fragment>
    );
};

export default Shipping;
