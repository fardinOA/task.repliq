/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    images: {
        unoptimized: true,
        domains: ["res.cloudinary.com", "external-preview.redd.it"],
    },
};

module.exports = nextConfig;
