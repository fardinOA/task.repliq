import { newOrder } from "./orderApi";

const { createAsyncThunk, createSlice } = require("@reduxjs/toolkit");

const initialState = {
    isLoading: false,
    error: "",
    isError: false,
    order: {},
};

export const addNewOrder = createAsyncThunk(
    "product/newOrder",
    async (data) => {
        const product = await newOrder(data);

        return product;
    }
);

// create slice

const productSlice = createSlice({
    name: "product",
    initialState,

    extraReducers: (builder) => {
        builder
            .addCase(addNewOrder.pending, (state, action) => {
                state.isError = false;
                state.isLoading = true;
            })
            .addCase(addNewOrder.fulfilled, (state, action) => {
                state.isError = false;
                state.isLoading = false;
                state.order = action.payload;
            })
            .addCase(addNewOrder.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
                state.order = {};
            });
    },
});

export default productSlice.reducer;
