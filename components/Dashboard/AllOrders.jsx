import Head from "next/head";
import React, { Fragment, useState } from "react";
import { useSelector } from "react-redux";

const AllOrders = ({
    showOrderDetails,
    setShowOrderDetails,
    setShowOrders,
    setOrderDetailsObj,
}) => {
    const { orders } = useSelector((state) => state.product.orders);

    return (
        <div className="    relative ">
            <Head>
                <title>Dashboard | Orders</title>
            </Head>
            <table className="table w-full h-full   ">
                <thead>
                    <tr className=" text-[17px] bg-[tomato] h-[5rem] ">
                        <th className=" text-left text-[16px] md:text-[19px] md:w-[20%] w-[12%] pl-2 ">
                            Order Id
                        </th>
                        <th className=" text-center text-[16px] md:text-[19px] md:w-[20%] w-[22%] ">
                            Status
                        </th>
                        <th className=" md:hidden text-center text-[16px] md:text-[19px] md:w-[20%]  w-[22%] ">
                            Qty
                        </th>
                        <th className=" hidden md:inline-block  text-[16px] pt-6  md:w-full h-full  md:text-[19px]  w-[22%] ">
                            Item Qty
                        </th>
                        <th className=" text-center text-[16px] md:text-[19px] md:w-[20%] w-[22%] ">
                            Amount
                        </th>
                        <th className=" text-center text-[16px] md:text-[19px] md:w-[20%] w-[22%] ">
                            Details
                        </th>
                    </tr>
                </thead>
                <tbody className=" w-full ">
                    {orders?.map((order, ind) => (
                        <Fragment key={`dashboard-allorder-${order._id}`}>
                            <tr className={`${ind % 2 ? "bg-red-100" : ""}`}>
                                <td className=" md:hidden pl-1 text-left py-1 text-[12px]  truncate  md:text-[16px] overflow-x-hidden   md:w-[20%] w-[12%]">
                                    {order?._id.slice(0, 15)}
                                </td>
                                <td className="hidden md:inline-block pl-1 text-left py-1 text-[12px]  w-full    md:text-[16px] overflow-x-hidden    ">
                                    {order?._id}
                                </td>
                                <td className=" text-center text-[12px] md:text-[16px] md:w-[20%] w-[22%]">
                                    {order?.orderStatus}
                                </td>
                                <td className=" text-center text-[12px] md:text-[16px] md:w-[20%] w-[22%]">
                                    {order?.orderItems?.length}
                                </td>
                                <td className=" text-center text-[12px] md:text-[16px] md:w-[20%] w-[22%]">
                                    {order?.totalPrice}
                                </td>
                                <td
                                    onClick={() => {
                                        setShowOrderDetails(!showOrderDetails);
                                        setShowOrders(false);
                                        setOrderDetailsObj(order);
                                    }}
                                    className=" cursor-pointer   md:w-[20%]    w-[22%]"
                                >
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeWidth={1.5}
                                        stroke="currentColor"
                                        className="w-6 h-6 mx-auto "
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                                        />
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                        />
                                    </svg>
                                </td>
                            </tr>
                        </Fragment>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default AllOrders;
