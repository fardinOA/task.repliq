import React from "react";

const ImageLoader = () => {
    return <div className="image-loader"></div>;
};

export default ImageLoader;
