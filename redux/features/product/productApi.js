import axios from "../../../utils/axios";

export const getAllProducts = async () => {
    const response = await axios.get("/products");
    return response.data;
};
export const getDashboardProducts = async () => {
    const response = await axios.get("/admin/products");
    return response.data;
};
export const getAllOrders = async () => {
    const response = await axios.get("/admin/orders");
    return response.data;
};

export const createProduct = async (productData) => {
    const config = {
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "*",
        },
    };
    axios.defaults.withCredentials = true;

    const { data } = await axios.post(
        `/admin/product/new`,
        productData,
        config
    );
    return data;
};
