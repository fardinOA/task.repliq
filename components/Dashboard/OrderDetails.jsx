import Image from "next/image";
import React, { Fragment } from "react";
import date from "date-and-time";
import Head from "next/head";
const OrderDetails = ({ order, setShowOrders, setShowOrderDetails }) => {
    return (
        <div className=" p-4  ">
            <Head>
                <title>Dashboard | Order | {order._id}</title>
            </Head>
            <h1
                onClick={() => {
                    setShowOrderDetails(false);
                    setShowOrders(true);
                }}
            >
                go back
            </h1>
            <h1 className="    text-[1.5rem] text-center font-bold font-mono   ">
                Ordered Items
            </h1>
            <table class="table w-full h-full   ">
                <thead>
                    <tr className=" text-[17px] bg-[tomato] h-[5rem] ">
                        <th className=" text-left text-[16px] md:text-[19px]  w-[30%] pl-2 ">
                            Order Id
                        </th>
                        <th className=" text-center text-[16px] md:text-[19px]  w-[30%] ">
                            Image
                        </th>
                        <th className=" text-center text-[16px] md:text-[19px]  w-[20%] ">
                            Name
                        </th>
                        <th className=" text-center text-[16px] md:text-[19px]  w-[20%] ">
                            Price
                        </th>
                    </tr>
                </thead>
                <tbody className=" w-full ">
                    {order?.orderItems?.map((item, ind) => (
                        <Fragment key={`order-details-${item._id}`}>
                            <tr className={`${ind % 2 ? "bg-red-100" : ""}`}>
                                <td className=" pl-1 text-left py-1 text-[12px] md:text-[16px] overflow-x-hidden w-[30%]">
                                    {item?._id}
                                </td>
                                <td className="     w-[30%]">
                                    <Image
                                        src={item?.image}
                                        alt={item?.name}
                                        height={100}
                                        width={100}
                                        className=" mx-auto object-contain w-[4rem] "
                                    />
                                </td>
                                <td className=" text-center text-[12px] md:text-[16px] w-[20%]">
                                    {item?.name}
                                </td>
                                <td className=" text-center text-[12px] md:text-[16px] w-[20%]">
                                    {item?.price}
                                </td>
                            </tr>
                        </Fragment>
                    ))}
                </tbody>
            </table>

            <div className="  flex md:flex-row flex-col    ">
                <div className="     w-full ">
                    <h1 className="  mt-4  text-[1.5rem]   text-center font-bold font-mono   ">
                        Payment Info
                    </h1>
                    <p className=" flex justify-around gap-8  ">
                        <span className=" text-right flex-1 ">Payment Id</span>{" "}
                        <span className=" truncate flex-1 ">
                            {order?.paymentInfo?.id}
                        </span>{" "}
                    </p>
                    <p className=" flex justify-around gap-8 ">
                        <span className=" text-right flex-1 ">
                            Payment Status
                        </span>{" "}
                        <span className=" flex-1 ">
                            {order?.paymentInfo?.status}
                        </span>
                    </p>
                    <p className=" flex justify-around gap-8 ">
                        <span className=" text-right flex-1 ">
                            Payment Date
                        </span>{" "}
                        <span className=" flex-1 ">
                            {date.format(
                                new Date(order?.paidAt),
                                "ddd, MMM DD YYYY"
                            )}
                        </span>
                    </p>
                </div>
                <div className=" w-full  ">
                    <h1 className="  mt-4  text-[1.5rem]   text-center font-bold font-mono   ">
                        Shipping Info
                    </h1>
                    <p className=" flex justify-around gap-8  ">
                        <span className=" text-right flex-1 ">Address</span>{" "}
                        <span className=" flex-1 ">
                            {order?.shippingInfo?.address}
                        </span>{" "}
                    </p>
                    <p className=" flex justify-around gap-8 ">
                        <span className=" text-right flex-1 ">City</span>{" "}
                        <span className=" flex-1 ">
                            {order?.shippingInfo?.city}
                        </span>
                    </p>
                    <p className=" flex justify-around gap-8 ">
                        <span className=" text-right flex-1 ">State</span>
                        <span className=" flex-1 ">
                            {order?.shippingInfo?.state}
                        </span>
                    </p>
                    <p className=" flex justify-around gap-8 ">
                        <span className=" text-right flex-1 ">Country</span>{" "}
                        <span className=" flex-1 ">
                            {order?.shippingInfo?.country}
                        </span>
                    </p>
                    <p className=" flex justify-around gap-8 ">
                        <span className=" text-right flex-1 ">Pincode</span>
                        <span className=" flex-1 ">
                            {order?.shippingInfo?.pinCode}
                        </span>
                    </p>
                    <p className=" flex justify-around gap-8 ">
                        <span className=" text-right flex-1 ">Phone No</span>
                        <span className=" flex-1 ">
                            {order?.shippingInfo?.phoneNo}
                        </span>
                    </p>
                    <p className=" flex justify-around gap-8 ">
                        <span className=" text-right flex-1 ">
                            Shipping cost
                        </span>
                        <span className=" flex-1 ">{order?.shippingPrice}</span>
                    </p>
                </div>
            </div>
        </div>
    );
};

export default OrderDetails;
