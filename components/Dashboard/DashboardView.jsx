import React from "react";

import ChartCmp from "./ChartCmp";
import { useSelector } from "react-redux";

const DashboardView = () => {
    const { products } = useSelector(
        (state) => state.product.dashboardProducts
    );
    return (
        <div className=" mt-8 flex flex-col  ">
            <h1 className=" text-[2rem] font-bold  md:text-[5rem]  border-b-8 border-blue-400  w-fit mx-auto ">
                Dashboard
            </h1>
            <div className=" my-8 mx-auto">
                <div className=" flex justify-center items-center transition-all duration-500 cursor-pointer hover:scale-110 bg-[#B4F8C8] text-[2rem] md:text-[4rem] rounded-full h-[10rem] w-[10rem] md:h-[15rem] md:w-[15rem] text-center pt-4 ">
                    <p className=" font-mono flex flex-col space-y-[-15px] md:space-y-[-25px] ">
                        {" "}
                        <span className=" text-[1.5rem] md:text-[3rem] ">
                            Products
                        </span>{" "}
                        <span>{products?.length}</span>
                    </p>
                </div>
            </div>
            <ChartCmp />
        </div>
    );
};

export default DashboardView;
