import React, { useEffect, useState } from "react";
import Carousel from "framer-motion-carousel";
import Head from "next/head";
import axios from "axios";

import StarRatings from "react-star-ratings";
import Image from "next/image";
import { useDispatch } from "react-redux";
import Details from "../../components/Product/Details";
const product = ({ product }) => {
    return (
        <div className=" w-[80%] lg:h-[80vh] mx-auto flex justify-center items-center  ">
            <Details product={product} />
        </div>
    );

    // return (
    //     <div className=" flex flex-col justify-between  md:flex-row ">
    //         <div className=" flex-1 flex justify-center items-center ">
    //             <Carousel interval={3000} renderDots={false}>
    //                 {product &&
    //                     product?.images?.map((ele, ind) => (
    //                         <Image
    //                             height={200}
    //                             width={200}
    //                             key={ele?.url}
    //                             src={ele?.url}
    //                             alt={`${ind + 1} Slide`}
    //                             className=" w-[70%] mx-auto p-8 "
    //                         />
    //                     ))}
    //             </Carousel>

    //             {/* {product &&
    //                     product.images?.map((ele, ind) => (
    //                         <Image
    //                             height={200}
    //                             width={200}
    //                             key={ele?.url}
    //                             src={ele?.url}
    //                             alt={`${ind + 1} Slide`}
    //                             className=" border-4 "
    //                         />
    //                     ))} */}
    //         </div>
    //         <div className=" bg-red-100  flex-1 ">
    //             <div className="">
    //                 <h2>{product?.name}</h2>
    //                 <p>Product # {product?._id}</p>
    //             </div>
    //             <div className="">
    //                 <StarRatings
    //                     rating={product?.ratings}
    //                     starDimension="15px"
    //                     starSpacing=""
    //                     starRatedColor="tomato"
    //                 />
    //                 <span className="">({product?.numOfReviews}) Reviews</span>
    //             </div>
    //             <div className="">
    //                 <h1>${product?.price}</h1>
    //                 <div className="">
    //                     <div className="">
    //                         <button
    //                             className="bg-gray-500 text-center rounded-[5px] p-2 py-1 text-[1rem] font-bold "
    //                             onClick={decreaseQuantity}
    //                         >
    //                             -
    //                         </button>
    //                         <input readOnly value={quantity} type="number" />
    //                         <button
    //                             className="bg-gray-500 rounded-[5px] p-2 py-1 text-[1rem] font-bold "
    //                             onClick={increaseQuantity}
    //                         >
    //                             +
    //                         </button>
    //                     </div>
    //                     <button
    //                         className="!p-2 md:ml-8  flex space-x-4 justify-center  !text-[15px] !rounded-[5px]"
    //                         disabled={product?.stock < 1 ? true : false}
    //                         onClick={addToCartHandeler}
    //                     >
    //                         <span> Add to Cart</span>
    //                         <span className=" "> </span>
    //                     </button>
    //                 </div>
    //                 <p>
    //                     Status:{" "}
    //                     <b
    //                         className={
    //                             product?.stock < 1 ? "redColor" : "greenColor"
    //                         }
    //                     >
    //                         {" "}
    //                         {product?.stock < 1 ? "OutOfStock" : "InStock"}
    //                     </b>
    //                 </p>
    //             </div>
    //             <div className="detailsBlock-4">
    //                 Description: <p>{product?.description}</p>
    //             </div>
    //             <button
    //                 onClick={submitReviewToggel}
    //                 className="!p-2 mt-4 flex space-x-4 justify-center  !text-[15px] !rounded-[5px] submitReview"
    //             >
    //                 <span>Submit Review</span>
    //                 <span> </span>
    //             </button>
    //         </div>
    //     </div>
    // );
};

export default product;

export async function getServerSideProps({ params }) {
    try {
        const res = await axios.get(
            `https://ecommerce-backend-fardinoa.vercel.app/api/v1/product/${params.slug}`
        );

        return { props: { product: res?.data?.product } };
    } catch (error) {
        console.log(error);
        return {
            redirect: {
                permanent: false,
                destination: `/`,
            },
        };
    }
}
