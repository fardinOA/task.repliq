import axios from "axios";

const axiosInstance = axios.create({
    baseURL: "https://ecommerce-backend-black.vercel.app/api/v1",
});

export default axiosInstance;
